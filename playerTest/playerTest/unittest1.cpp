#include "stdafx.h"
#include "CppUnitTest.h"
#include "..\gameProject\character.h"
#include "..\gameProject\PlayerCharacter.h"
#include "..\gameProject\NonPlayerCharacter.h"
#include "..\gameProject\Questions.h"
#include "..\gameProject\Answers.h"
#include "..\gameProject\Knowledge.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace playerTest
{		
	TEST_CLASS(UnitTest1)
	{
	public:

		TEST_METHOD(TestMethod1)
		//**Check the class exists and has been implemented correctly.
		{
			// TODO: Your test code here
			CCharacter sg(0);
			Assert::IsNotNull(&sg);
		}

		TEST_METHOD(CharacterReturnType)
		//**Check the values returned from the class are as expected.  
		{
			CCharacter sg(0);
			int type = sg.GetType();
			Assert::AreEqual(0, type);
			CCharacter *sg1 = new CCharacter(1);
			type = sg1->GetType();
			Assert::AreEqual(1, type);
		}

		//TEST_METHOD(PlayerCharacterReturnType)
		//**Check the age and name of the correct match up correctly to the given parameters.
		//{
		//	const std::string PLAYER_NAME = "Test";
		//	const int PLAYER_AGE = 21;
		//	CPlayerCharacter testPlayer(PLAYER_NAME, PLAYER_AGE);
		//	std::string playerName = testPlayer.GetName(); //gets the method GetName from PlayerCharacter class from the variable testPlayer and passes in test and 21 to the class
		//	int playerAge = testPlayer.GetAge();
		//	Assert::AreEqual(PLAYER_NAME, playerName);
		//	Assert::AreEqual(PLAYER_AGE, playerAge);
		//}

		//TEST_METHOD(NonPlayerCharacterReturnType)
		//**Check the age and name of the correct match up correctly to the given parameters.
		//{
		//	const std::string NONPLAYER_NAME = "Test";
		//	const int NONPLAYER_AGE = 21;
		//	CNonPlayerCharacter testNonPlayer(NONPLAYER_NAME, NONPLAYER_AGE);
		//	std::string nonPlayerName = testNonPlayer.GetName();
		//	int nonPlayerAge = testNonPlayer.GetAge();
		//	Assert::AreEqual(NONPLAYER_NAME, nonPlayerName);
		//	Assert::AreEqual(NONPLAYER_AGE, nonPlayerAge);
		//}

		//TEST_METHOD(CharacterNameList)
		//**Wrote test to check the member variables were updating. Test passed but was not a good test. 
		//{
		//	CCharacter::m_ilIndex;
		//	CCharacter::m_slNames;
		//	Assert::IsNotNull(&m_ilIndex);
		//	Assert::IsNotNull(&m_slNames);
		//}

		//TEST_METHOD(CharacterNameList)
		////**Check the list actually exists.
		//{
		//	CCharacter list;
		//	list.PopulateCharacterNameList();
		//	Assert::IsFalse(list.m_slNames.empty());
		//}

		TEST_METHOD(PlayerCharacterGetName)
		//**Gives the player name from an index number from the character list and checks it matches.
		{
			const std::string PLAYER_NAME = "Test"; 
			const int PLAYER_NAME_LIST_INDEX = 0;
			CPlayerCharacter testNameList;
			testNameList.PopulateCharacterNameList();
			std::string playerName = testNameList.GetName(PLAYER_NAME_LIST_INDEX);
			Assert::AreEqual(PLAYER_NAME, playerName);
		}

		TEST_METHOD(PlayerCharacterIndexReturn)
		//**Checks the member variable name index is correct.
		{
			const std::string PLAYER_NAME = "Mrs. Peacock";
			const int PLAYER_NAME_LIST_INDEX = 3;
			CPlayerCharacter testPlayer;
			testPlayer.PopulateCharacterNameList();
			testPlayer.GetName(PLAYER_NAME_LIST_INDEX);
			int playerIndex = testPlayer.GetIndex();
			Assert::AreEqual(PLAYER_NAME_LIST_INDEX, playerIndex);
		}

		TEST_METHOD(NonPlayerCharacterNamesListReturn)
		//**Checks all non Player Characters names (all the names apart from the player name) are saved into Non Player Name List.
		{
			const std::string PLAYER_NAME = "Reverend Green";
			const int PLAYER_NAME_LIST_INDEX = 4;
			CPlayerCharacter testPlayer;
			testPlayer.PopulateCharacterNameList();
			testPlayer.GetName(PLAYER_NAME_LIST_INDEX);

			CNonPlayerCharacter testNonPlayer;
			testNonPlayer.GetNPNames(PLAYER_NAME_LIST_INDEX);
			Assert::IsTrue(testNonPlayer.m_slNPNames.empty());
		}

		TEST_METHOD(TestMethodQuestionsClass)
			//**Check the class exists and has been implemented correctly.
		{
			CQuestions ques(0);
			int type = ques.GetType();
			Assert::AreEqual(0, type);
			CQuestions *ques1 = new CQuestions(1);
			type = ques1->GetType();
			Assert::AreEqual(1, type);
		}

		TEST_METHOD(QuestionsList)
		//**Check the list actually exists.
		{
			CQuestions questions;
			questions.PopulateQuestionList();
			Assert::IsFalse(questions.m_slQuestions.empty());
		}

		TEST_METHOD(TestMethodAnswersClass)
			//**Check the class exists and has been implemented correctly.
		{
			CAnswers answers(0);
			int type = answers.GetType();
			Assert::AreEqual(0, type);
			CAnswers *answers1 = new CAnswers(1);
			type = answers1->GetType();
			Assert::AreEqual(1, type);
		}


		TEST_METHOD(AnswerList)
			//**Check the list actually exists.
		{
			CAnswers answers;
			answers.PopulateAnswerList();
			Assert::IsFalse(answers.m_slAnswers.empty());
		}

		TEST_METHOD(KnowledgeReturnType)
			//**Check the values returned from the class are as expected.  
		{
			CKnowledge know(0);
			int type = know.GetType();
			Assert::AreEqual(0, type);
			CKnowledge *know1 = new CKnowledge(1);
			type = know1->GetType();
			Assert::AreEqual(1, type);
		}
		
		TEST_METHOD(KnowledgeList)
			//**insert name, question, answer index numbers into the knowledge list
		{
			const int NAME_INDEX = 0;
			const int QUESTION_INDEX = 1;
			const int ANSWER_INDEX = 2;
			CKnowledge knowledge;
			knowledge.AddKnowledgeList(NAME_INDEX, QUESTION_INDEX, ANSWER_INDEX);
			Assert::IsFalse(knowledge.m_nqaKnowledgeVectorArray.empty());
		}

		TEST_METHOD(ReturnQuestion)
		{
			const int PLAYER_QUESTION = 0;
			const std::string PLAYER_QUESTION_STRING = "test questions?";
			CQuestions questions;
			questions.PopulateQuestionList();
			std::string playerQuestion = questions.ReturnQuestion(PLAYER_QUESTION);		
			Assert::AreEqual(PLAYER_QUESTION_STRING, playerQuestion);
		}
	};
}
