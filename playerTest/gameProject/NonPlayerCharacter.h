#pragma once
#include "character.h"
#include <string> 

class GAMEPROJECT_API CNonPlayerCharacter :
	public CCharacter
{
public:
	CNonPlayerCharacter();
	std::string m_sNPName;
	int m_iNPIndex; 
	std::list <std::string> GetNPNames(int pIndex);
	int GetIndex();
};

