#include "stdafx.h"
#include "PlayerCharacter.h"
#include "Questions.h"
#include <string> 
#include <fstream>
#include <list>
#include <sstream>


//CPlayerCharacter::CPlayerCharacter(std::string name, int index) //add parameters so every character will get these
//	: m_sPName(name), m_iPIndex(index)  //after : an initialisation list is created before and code in the main body is executed. So it's all made ready to be used
//{
//}

CPlayerCharacter::CPlayerCharacter() 
{
}

std::string CPlayerCharacter::GetName(int nameIndex)
//**Method to retrieve name from Character List. The game user would be able to choose their charcater name from the available characters.
{
	m_iPIndex = nameIndex;
	std::fstream testFile; //create a test file to print output to for error checking
	testFile.open("\\testName.txt", std::fstream::in | std::fstream::out | std::fstream::app);
	
	std::string name = "Default Name";
	int counter = 0;
	for (std::list<std::string>::const_iterator nameIterator = m_slNames.begin(); nameIterator != m_slNames.end(); nameIterator++)
	{
		if (counter == nameIndex)
			{	            
				name = *nameIterator;
				//testFile << "Found: " << name << " at " << counter << "\n";
				//testFile.close();
				m_sPName = name;
				nameIterator = m_slNames.begin();
				return m_sPName;
			}
		counter++;
	}
	return name;
}

int CPlayerCharacter::GetIndex()
{
	return m_iPIndex;
}

std::string CPlayerCharacter::GetQuestion(int question)
{
	CQuestions questionReturned;
	std::string new_question = questionReturned.ReturnQuestion(question);
	return new_question;
}
