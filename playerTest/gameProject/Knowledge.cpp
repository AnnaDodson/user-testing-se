#pragma once
#include "stdafx.h"
#include "Knowledge.h"
#include <fstream>
#include <string>
#include <vector>
#include <array>


CKnowledge::CKnowledge()
{
	m_iType = 0;
}

CKnowledge::~CKnowledge()
{
}


CKnowledge::CKnowledge(int t)
{
	m_iType = t;
}


int CKnowledge::GetType()
{
	return m_iType;
}

void CKnowledge::AddKnowledgeList(int name, int question, int answer)
{
	std::array<int, 3 > nqaArray = { name, question, answer };
	m_nqaKnowledgeVectorArray.push_back(nqaArray);
}