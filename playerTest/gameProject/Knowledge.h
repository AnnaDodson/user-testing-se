#pragma once
#ifdef GAMEPROJECT_EXPORTS
#define GAMEPROJECT_API __declspec(dllexport)
#else
#define GAMEPROJECT_API __declspec(dllimport)
#endif
#include <fstream>
#include <string> 
#include <vector>
#include <array>

typedef std::vector<int> intVector;
typedef std::vector<std::string> stringVector;

typedef std::vector<std::array<int, 3>> nqaVectorArray;

class GAMEPROJECT_API CKnowledge
{
public:
	CKnowledge();
	~CKnowledge();
	CKnowledge(int t);
	int m_iType;
	int GetType();
	void AddKnowledgeList(int name, int question, int answer);
	nqaVectorArray m_nqaKnowledgeVectorArray;
	stringVector m_slKnowledge;
	intVector m_ilKnowledgeIndex;
};

