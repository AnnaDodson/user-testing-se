#pragma once
#ifdef GAMEPROJECT_EXPORTS
#define GAMEPROJECT_API __declspec(dllexport)
#else
#define GAMEPROJECT_API __declspec(dllimport)
#endif
#include <fstream>
#include <string> 
#include <vector>

typedef std::vector<int> intVector;
typedef std::vector<std::string> stringVector;

class GAMEPROJECT_API CAnswers
{
public:
	CAnswers();
	CAnswers(int t);
	int m_iType;
	int GetType();
	void PopulateAnswerList();
	stringVector m_slAnswers;
	intVector m_ilAnswersIndex;
};

