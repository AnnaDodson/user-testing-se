#pragma once
// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the GAMEPROJECT_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// GAMEPROJECT_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef GAMEPROJECT_EXPORTS
#define GAMEPROJECT_API __declspec(dllexport)
#else
#define GAMEPROJECT_API __declspec(dllimport)
#endif
#include <string>
#include <list>

// This class is exported from the gameProject.dll
class GAMEPROJECT_API CCharacter {
protected: 
	int m_iType;
	std::string m_sPName;
	int m_iPIndex;
	std::string m_sNPName;
	int m_iNPIndex;
public:
	CCharacter(void);
	CCharacter(int t);
	std::list <std::string> m_slNames;
	std::list <std::string> m_slNPNames;
	std::list <int> m_ilIndex;
	std::list <std::string>::const_iterator m_slcNames;
	std::list <int>::const_iterator m_ilcIndex;
	int GetType();
	void PopulateCharacterNameList();
};

extern GAMEPROJECT_API int ngameProject;

GAMEPROJECT_API int fngameProject(void);
