#include "stdafx.h"
#include "Answers.h"


CAnswers::CAnswers()
{
	m_iType = 0;
}

CAnswers::CAnswers(int t)
{
	m_iType = t;
}


int CAnswers::GetType()
{
	return m_iType;
}

void CAnswers::PopulateAnswerList()
{
	std::fstream testList;
	testList.open("\\testName.txt", std::fstream::in | std::fstream::out | std::fstream::app);

	std::ifstream inputAnswerFile;
	inputAnswerFile.open("Z:\\answers.txt");
	if (!inputAnswerFile)
	{
		testList << "something went wrong :( " << "\n";
		//throw invalid_argument("File Not Found");
	}
	else
	{
		std::string line;
		while (getline(inputAnswerFile, line))
		{
			int endColumnNumber = line.find(',');
			std::string answerSIndex = line.substr(0, endColumnNumber);
			int amswerNumber = atoi(answerSIndex.c_str());
			m_ilAnswersIndex.push_back(amswerNumber);

			//testList << "Found answers: " << amswerNumber << "\n";

			int endColumn = line.length();
			std::string answer = line.substr(endColumnNumber + 1, endColumn - endColumn);
			m_slAnswers.push_back(answer);
		}
		inputAnswerFile.close();
		testList.close();
	}
}