#pragma once
#include "character.h"
#include "Questions.h"
#include <string>
#include <list>

class GAMEPROJECT_API CPlayerCharacter :
	public CCharacter
{
public:
	CPlayerCharacter(std::string name, int m_iIndex);
	CPlayerCharacter();
	//std::string m_sPName;
	//int m_iPIndex;
	std::string GetName(int nameIndex);
	int GetIndex();
	//std::string GetQuestion(int question);
};