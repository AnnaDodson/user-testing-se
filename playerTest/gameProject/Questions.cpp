#include "stdafx.h"
#include "Questions.h"
#include <fstream>
#include <string> 
#include <vector>
#include "stdexcept"

CQuestions::CQuestions()
{
	m_iType = 0;
}

CQuestions::CQuestions(int t)
{
	m_iType = t;
}


int CQuestions::GetType()
{
	return m_iType;
}

void CQuestions::PopulateQuestionList()
{
	std::fstream testList;
	testList.open("\\testName.txt", std::fstream::in | std::fstream::out | std::fstream::app);

	std::ifstream inputQuestionFile;
	inputQuestionFile.open("Z:\\questions.txt");
	if (!inputQuestionFile)
	{
		testList << "something went wrong :( " << "\n";
		//throw invalid_argument("File Not Found");
	}
	else
	{
		std::string line;
		while (getline(inputQuestionFile, line))
		{
			int endColumnNumber = line.find(',');
			std::string questionSIndex = line.substr(0, endColumnNumber);
			int questionNumber = atoi(questionSIndex.c_str());
			m_ilQuestionsIndex.push_back(questionNumber);

			testList << "Found Questions: " << questionNumber << "\n";

			int endColumn = line.length();
			std::string question = line.substr(endColumnNumber + 1, endColumn - endColumn);
			m_slQuestions.push_back(question);
		}
		inputQuestionFile.close();
		testList.close();
	}
}

std::string CQuestions::ReturnQuestion(int question)
//**Method to return the question from the questions list bu using the index number
{
	std::string new_question;
	new_question = m_slQuestions.at(question);
	return new_question;
}

	