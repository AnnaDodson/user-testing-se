
#include "stdafx.h"
#include "character.h"
#include <string> 
#include <fstream>
#include <list>

//TODO I think I don't need this, can be removed if not needed 
//// This is an example of an exported variable
//GAMEPROJECT_API int ngameProject=0;
//
//
//// This is an example of an exported function.
//GAMEPROJECT_API int fngameProject(void)
//{
//	return 42;
//}



// This is the constructor of a class that has been exported.
// see CCharacter.h for the class definition
CCharacter::CCharacter()
{
	m_iType = 0;
	return;
}

CCharacter::CCharacter(int t){
	m_iType = t;
}

int CCharacter::GetType()
{
	return m_iType;
}

void CCharacter::PopulateCharacterNameList()
{
	std::fstream testList;
	testList.open("\\testName.txt", std::fstream::in | std::fstream::out | std::fstream::app);

	std::ifstream inputCharacterFile;
	inputCharacterFile.open("\\characterList.txt");
	if (!inputCharacterFile)
	{
		testList << "something went wrong :(: " << "\n";
		//TODO if file doesn't work handle the exception
	}
	else
	{
		std::string line;
		while (getline(inputCharacterFile, line))
		{
			int endColumnName = line.find(',');
			std::string name = line.substr(0, endColumnName);
			m_slNames.push_back(name);

			//testList << "Found: " << name << "\n";

			int endColumnAge = line.length();
			std::string sIndex = line.substr(endColumnName + 1, endColumnAge - endColumnName);
			int index = atoi(sIndex.c_str());
			m_ilIndex.push_back(index);
		}
	}
	inputCharacterFile.close();
	m_slcNames = m_slNames.begin(); //sets the iterator to the first item in the list
	m_ilcIndex = m_ilIndex.begin();
	testList.close();
}
