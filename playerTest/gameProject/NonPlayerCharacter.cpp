#include "stdafx.h"
#include "NonPlayerCharacter.h"
#include <string>
#include <fstream>
#include <list>
#include <sstream>


CNonPlayerCharacter::CNonPlayerCharacter()
{
}

std::list<std::string> CNonPlayerCharacter::GetNPNames(int pIndex)
//**Method to retrieve names of Non Characters. The Non-Player names are every name apart from the Player Name.
{

	std::fstream testFile; //create a test file to print output to for error checking
	testFile.open("\\testName.txt", std::fstream::in | std::fstream::out | std::fstream::app);

	std::string nPName = "Default Name1";
	std::list <std::string> lNPName;
	lNPName.push_back(nPName);
	int counter = 0;
	for (std::list<std::string>::const_iterator nPNameIterator = m_slNames.begin(); nPNameIterator != m_slNames.end(); nPNameIterator++)
	{

		if (counter != pIndex)
		{
			nPName = *nPNameIterator;
			m_slNPNames.push_back(nPName);
			return m_slNPNames;
		}
		counter++;
	}
	return lNPName;
}

int CNonPlayerCharacter::GetIndex()
{
	return m_iNPIndex;
}
