#pragma once
#include <fstream>
#include <string> 
#include <vector>
#include "stdexcept"
#ifdef GAMEPROJECT_EXPORTS
#define GAMEPROJECT_API __declspec(dllexport)
#else
#define GAMEPROJECT_API __declspec(dllimport)
#endif

typedef std::vector<int> intVector;
typedef std::vector<std::string> stringVector;

class GAMEPROJECT_API CQuestions
{
public:
	CQuestions();
	CQuestions(int t);
	int m_iType;
	int GetType();
	void PopulateQuestionList();
	stringVector m_slQuestions;
	intVector m_ilQuestionsIndex;
	std::string CQuestions::ReturnQuestion(int question);
};

